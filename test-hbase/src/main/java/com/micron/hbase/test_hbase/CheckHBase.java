package com.micron.hbase.test_hbase;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.HBaseAdmin;
import org.apache.hadoop.hbase.client.HTable;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.filter.PageFilter;
import org.apache.hadoop.hbase.util.Bytes;

/**
 * Hello world!
 *
 */
public class CheckHBase 
{
    public static void main( String[] args ) throws IOException
    {
        System.out.println( "Hello World!" );
        Configuration config = HBaseConfiguration.create();

        // Example of setting zookeeper values for HDInsight
        // in code instead of an hbase-site.xml file
        //
        config.set("hbase.zookeeper.quorum",
                    "10.0.1.127");
        config.set("hbase.zookeeper.property.clientPort", "2181");
        config.set("hbase.cluster.distributed", "true");
        // The following sets the znode root for Linux-based HDInsight
        //config.set("zookeeper.znode.parent","/hbase-unsecure");

        // create an admin object using the config
        // HBaseAdmin admin = new HBaseAdmin(config);
        HTableDescriptor table = new HTableDescriptor("User"); 
        // HTable table = new HTable(config, "rawdata_hoang");
        
        table.addFamily( new HColumnDescriptor("Id"));
        table.addFamily( new HColumnDescriptor("Name"));
        System.out.println( "Connecting..." );
        HBaseAdmin hbase_admin = new HBaseAdmin( config );
        System.out.println( "Creating Table..." );
        hbase_admin.createTable( table );
        System.out.println("Done!");
        /* 
        Scan scan = new Scan(Bytes.toBytes("ch-"));
        scan.setFilter(new PageFilter(250));

        ResultScanner scanner = table.getScanner(scan);

        for (Result result : scanner) {

            // ...
        	System.out.println(result);

        }
        */
        /*
        Get get = new Get(Bytes.toBytes("chvO7GeLq8:r201:2017-02-15-07:17:58:127.0.1.1"));

        get.addFamily(Bytes.toBytes("cf"));

        get.setMaxVersions(3);

        Result result = table.get(get);
        System.out.println(result);
        byte[]         value     = result.getValue(Bytes.toBytes("cf"), Bytes.toBytes("SPC"));
        if (null != value) {
			int spc = Integer.valueOf(new String(value));
			System.out.println("SPC: " + spc);
			
		}
		*/
        
        // table.close();
    }
}
