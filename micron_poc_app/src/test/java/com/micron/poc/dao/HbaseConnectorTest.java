package com.micron.poc.dao;

import java.io.IOException;

import org.apache.hadoop.hbase.MasterNotRunningException;
import org.apache.hadoop.hbase.ZooKeeperConnectionException;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.google.protobuf.ServiceException;
import com.micron.poc.kinesis.model.Message;
import com.micron.poc.utils.Constants;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class HbaseConnectorTest {

	@Test
	public void test1CheckConnectionIsAvailble() throws MasterNotRunningException, ZooKeeperConnectionException, ServiceException, IOException {
		HbaseConnector connector = HbaseConnector.getInstance();
		boolean res = connector.checkConnectionIsAvailble();
		Assert.assertTrue(res);
	}
	
	//@Test
	public void test2Put() throws MasterNotRunningException, ZooKeeperConnectionException, ServiceException, IOException {
		
	}

}
