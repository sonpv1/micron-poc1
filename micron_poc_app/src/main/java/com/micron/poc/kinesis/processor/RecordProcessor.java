package com.micron.poc.kinesis.processor;/*
 * Copyright 2012-2017 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 *  http://aws.amazon.com/apache2.0
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.amazonaws.services.cloudwatch.AmazonCloudWatch;
import com.amazonaws.services.cloudwatch.AmazonCloudWatchClientBuilder;
import com.amazonaws.services.cloudwatch.model.MetricDatum;
import com.amazonaws.services.cloudwatch.model.PutMetricDataRequest;
import com.amazonaws.services.kinesis.clientlibrary.exceptions.InvalidStateException;
import com.amazonaws.services.kinesis.clientlibrary.exceptions.ShutdownException;
import com.amazonaws.services.kinesis.clientlibrary.exceptions.ThrottlingException;
import com.amazonaws.services.kinesis.clientlibrary.interfaces.IRecordProcessor;
import com.amazonaws.services.kinesis.clientlibrary.interfaces.IRecordProcessorCheckpointer;
import com.amazonaws.services.kinesis.clientlibrary.lib.worker.ShutdownReason;
import com.amazonaws.services.kinesis.model.Record;
import com.micron.poc.dao.DynamoDBConnector;
import com.micron.poc.dao.HbaseConnector;
import com.micron.poc.kinesis.model.Message;
import com.micron.poc.utils.Constants;

/**
 * Processes records and checkpoints progress.
 */
public class RecordProcessor implements IRecordProcessor {

    private static final Log LOG = LogFactory.getLog(RecordProcessor.class);

    private String kinesisShardId;
    private AmazonCloudWatch client;
    // Backoff and retry settings
    private static final long BACKOFF_TIME_IN_MILLIS = 3000L;
    private static final int  NUM_RETRIES            = 10;

    // Checkpoint about once a minute
    private long nextCheckpointTimeInMillis;

    private final        CharsetDecoder    decoder                    = Charset.forName("UTF-8").newDecoder();
    private static final DynamoDBConnector DYNAMO_DB_CONNECTOR        = DynamoDBConnector.getInstance();
    private static final String            TOTAL_RECORD               = "totalRecord";
    private static final long              CHECKPOINT_INTERVAL_MILLIS = 60000L;
    private static final HbaseConnector    HBASE_CONNECTOR            = HbaseConnector.getInstance();
    private static 		 long 			   NUM_PROCESSED_MESAGES      = 0;

    /**
     * {@inheritDoc}
     */
    public void initialize(String shardId) {
        LOG.info("Initializing record processor for shard: " + shardId);
        this.kinesisShardId = shardId;
        
		client =  AmazonCloudWatchClientBuilder.defaultClient();
    }

    /**
     * {@inheritDoc}
     */
    public void processRecords(List<Record> records, IRecordProcessorCheckpointer checkpointer) {
        LOG.info("Processing " + records.size() + " records from " + kinesisShardId);

        // Process records and perform all exception handling.
        processRecordsWithRetries(records);

        // Checkpoint once every checkpoint interval.
        if (System.currentTimeMillis() > nextCheckpointTimeInMillis) {
            checkpoint(checkpointer);
            nextCheckpointTimeInMillis = System.currentTimeMillis() + CHECKPOINT_INTERVAL_MILLIS;
        }
    }

    /**
     * Process records performing retries as needed. Skip "poison pill" records.
     * 
     * @param records Data records to be processed.
     */
    private void processRecordsWithRetries(List<Record> records) {
        for (Record record : records) {
            boolean processedSuccessfully = false;
            for (int i = 0; i < NUM_RETRIES; i++) {
                try {
                    processSingleRecord(record);
                    processedSuccessfully = true;
                    NUM_PROCESSED_MESAGES ++;
                    if(NUM_PROCESSED_MESAGES % 200 == 0) {
                    	LOG.info("TOTAL NUMBER OF PROCESSED MESSAGES: " + NUM_PROCESSED_MESAGES);
                    	PutMetricDataRequest metricReq = new PutMetricDataRequest();
						metricReq.withNamespace("MicronMetrics");
                    	metricReq.withMetricData(new MetricDatum().withMetricName("MessagesConsumed").withTimestamp(new Date()).withValue(200.0));
                    	client.putMetricData(metricReq);
                    }
                    break;
                } catch (Throwable t) {
                    LOG.warn("Caught throwable while processing record " + record, t);
                }
                // backoff if we encounter an exception.
                try {
                    Thread.sleep(BACKOFF_TIME_IN_MILLIS);
                } catch (InterruptedException e) {
                    LOG.debug("Interrupted sleep", e);
                }
            }

            if (!processedSuccessfully) {
                LOG.error("Couldn't process record " + record + ". Skipping the record.");
            }
        }
    }

    /**
     * Process a single record.
     * 
     * @param record The record to be processed.
     */
    private void processSingleRecord(Record record) {
    	String data = null;
        try {
        	ByteBuffer buffer = record.getData();
        	data = decoder.decode(buffer).toString();
            Message msg = Message.fromJsonAsBytes(buffer.array());
            if (null != msg) {
            	//LOG.info("Processing msg for " + msg.getRowKey());
                // long hasRowKey = DYNAMO_DB_CONNECTOR.getRowKeyCount(msg.getRowKey());
                // LOG.info("Processing msg for " + msg.getRowKey() + " counter: " + String.valueOf(hasRowKey));
                DYNAMO_DB_CONNECTOR.increase(msg.getRowKey());
                /*
                if (hasRowKey == -1) {
                    DYNAMO_DB_CONNECTOR.increase(TOTAL_RECORD);
                }
				*/
                // SnsTopicPublisher.publishToTopic(msg.getRowKey());
                HBASE_CONNECTOR.put(Constants.HBASE_TABLE_NAME, msg);
                //LOG.info(record.getSequenceNumber() + ", " + record.getPartitionKey() + ", " + msg);
            }
        } catch (Exception e) {
            LOG.info("Record does not match sample record format. Ignoring record with data; " + data + " ;exception message: " + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * {@inheritDoc}
     */
    public void shutdown(IRecordProcessorCheckpointer checkpointer, ShutdownReason reason) {
        LOG.info("Shutting down record processor for shard: " + kinesisShardId);
        // Important to checkpoint after reaching end of shard, so we can start processing data from child shards.
        if (reason == ShutdownReason.TERMINATE) {
            checkpoint(checkpointer);
        }
    }

    /** Checkpoint with retries.
     * @param checkpointer
     */
    private void checkpoint(IRecordProcessorCheckpointer checkpointer) {
        LOG.info("Checkpointing shard " + kinesisShardId);
        for (int i = 0; i < NUM_RETRIES; i++) {
            try {
                checkpointer.checkpoint();
                break;
            } catch (ShutdownException se) {
                // Ignore checkpoint if the processor instance has been shutdown (fail over).
                LOG.info("Caught shutdown exception, skipping checkpoint.", se);
                break;
            } catch (ThrottlingException e) {
                // Backoff and re-attempt checkpoint upon transient failures
                if (i >= (NUM_RETRIES - 1)) {
                    LOG.error("Checkpoint failed after " + (i + 1) + "attempts.", e);
                    break;
                } else {
                    LOG.info("Transient issue when checkpointing - attempt " + (i + 1) + " of "
                            + NUM_RETRIES, e);
                }
            } catch (InvalidStateException e) {
                // This indicates an issue with the DynamoDB table (check for table, provisioned IOPS).
                LOG.error("Cannot save checkpoint to the DynamoDB table used by the Amazon Kinesis Client Library.", e);
                break;
            }
            try {
                Thread.sleep(BACKOFF_TIME_IN_MILLIS);
            } catch (InterruptedException e) {
                LOG.debug("Interrupted sleep", e);
            }
        }
    }
}
