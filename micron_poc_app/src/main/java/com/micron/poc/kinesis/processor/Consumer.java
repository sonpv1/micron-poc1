package com.micron.poc.kinesis.processor;


import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.services.kinesis.clientlibrary.interfaces.IRecordProcessorFactory;
import com.amazonaws.services.kinesis.clientlibrary.lib.worker.InitialPositionInStream;
import com.amazonaws.services.kinesis.clientlibrary.lib.worker.KinesisClientLibConfiguration;
import com.amazonaws.services.kinesis.clientlibrary.lib.worker.Worker;
import com.micron.poc.utils.PropertyLoader;

import java.util.UUID;

/**
 * Consumer
 */
public final class Consumer {

    private static final String                  APPLICATION_NAME           = "MICRON-SIGMAZ2";
    private static final String                  STREAM_NAME                = PropertyLoader.getInstance().getPropValue("streamName");
    private static final String                  REGION_NAME                = PropertyLoader.getInstance().getPropValue("regionName");
    private static final InitialPositionInStream INITIAL_POSITION_IN_STREAM = InitialPositionInStream.LATEST;

    public static void main(String[] args) throws Exception {
    	System.out.println("client start running");
        KinesisClientLibConfiguration kinesisClientLibConfiguration = new KinesisClientLibConfiguration(
                // APPLICATION_NAME + System.currentTimeMillis(), STREAM_NAME,
        		APPLICATION_NAME, STREAM_NAME,
                new DefaultAWSCredentialsProviderChain(),"" +
                "micron-" + UUID.randomUUID().toString()).
                withRegionName(REGION_NAME).
                withInitialPositionInStream(InitialPositionInStream.TRIM_HORIZON).
                withInitialPositionInStream(INITIAL_POSITION_IN_STREAM);

        IRecordProcessorFactory recordProcessorFactory = new RecordProcessorFactory();
        Worker                  worker                 = new Worker(recordProcessorFactory, kinesisClientLibConfiguration);

        int exitCode = 0;
        try {
            worker.run();
        } catch (Throwable t) {
            System.err.println("Caught throwable while processing data.");
            t.printStackTrace();
            exitCode = 1;
        }
        System.exit(exitCode);
    }

}
