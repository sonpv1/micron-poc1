package com.micron.poc.dao;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

import com.micron.poc.utils.PropertyLoader;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.MasterNotRunningException;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.ZooKeeperConnectionException;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.HBaseAdmin;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.util.Bytes;

import com.amazonaws.services.cloudwatch.AmazonCloudWatch;
import com.amazonaws.services.cloudwatch.AmazonCloudWatchClientBuilder;
import com.amazonaws.services.cloudwatch.model.MetricDatum;
import com.amazonaws.services.cloudwatch.model.PutMetricDataRequest;
import com.google.protobuf.ServiceException;
import com.micron.poc.kinesis.model.Message;

public class HbaseConnector {
	private static final Log LOG = LogFactory.getLog(HbaseConnector.class);
	private static final String            HBASE_MASTER_HOST      = PropertyLoader.getInstance().getPropValue("hbasemasterhost");
	private static final String            HBASE_MASTER_IP      = PropertyLoader.getInstance().getPropValue("hbasemasterip");
	
	private AmazonCloudWatch client;
	
	private static HbaseConnector instance = null;
	Configuration config;
	
	private Connection conn;
	
	private final        BlockingQueue<Message> CACHE   = new LinkedBlockingQueue<>();
	
	private ExecutorService threadPool;

	private HbaseConnector() {
		config = HBaseConfiguration.create();
		config.set("hbase.zookeeper.quorum", HBASE_MASTER_HOST);
		config.set("hbase.zookeeper.property.clientPort", "2181");
		config.set("hbase.master", HBASE_MASTER_IP);
		
		try {
			conn = ConnectionFactory.createConnection(config);
		} catch (IOException ioe) {
			System.out.println(ioe);
		}
		
		threadPool = Executors.newFixedThreadPool(4);
		client =  AmazonCloudWatchClientBuilder.defaultClient();

		// CompletionService<String> pool = new ExecutorCompletionService<String>(threadPool);
	}

	public static HbaseConnector getInstance() {
		if (instance == null) {
			instance = new HbaseConnector();
		}
		return instance;
	}

	public boolean checkConnectionIsAvailble() throws MasterNotRunningException, ZooKeeperConnectionException, ServiceException, IOException {

		HBaseAdmin.checkHBaseAvailable(config);
		System.out.println("HBase is running!");
		return true;
	}
	
	public void put(String strTable, Message mes) throws IOException {
		
		final TableName tableName = TableName.valueOf(strTable);
		
		
		
		Admin admin = conn.getAdmin();
		/*
	    if (!admin.tableExists(tableName)) {
	        admin.createTable(new HTableDescriptor(tableName).addFamily(new HColumnDescriptor("cf")));
	    }
	    */
		
		// FIXME: I need to fix the logic here.
		// The Hbase logic should trigger the DynamoDB logic. --> so when we flush the Hbase cache,
		// we should also flush the DynamoDB cache. --> the counter will be consistent with Hbase data.
		CACHE.add(mes);
		
		if (CACHE.size() > 1000) {
			LOG.info("Flushing records to HBase");
			final ArrayList<Message> toGoMessages = new ArrayList<>(CACHE.size());
			synchronized (CACHE) {
				CACHE.drainTo(toGoMessages);
			}
			// Now move on to push these messages to HBase
			threadPool.execute(new Runnable() {
				
				@Override
				public void run() {
					// TODO Auto-generated method stub
					try {
						Table table = conn.getTable(tableName);
						List<Put> batchPuts = new ArrayList<Put>(toGoMessages.size());
						for (Message message: toGoMessages) {
						    Put p = new Put(Bytes.toBytes(message.getRowKey()));
						    p.addColumn(Bytes.toBytes("cf"), Bytes.toBytes(message.getAttribute()), Bytes.toBytes(message.getValue()));
						    batchPuts.add(p);
						    // LOG.info("message id: " + message.getRowKey() + " attribute: " + message.getAttribute());
						}
					    table.put(batchPuts);
	
					    table.close();
					    LOG.info("FINISHED Flushing records to HBase");
					    PutMetricDataRequest metricReq = new PutMetricDataRequest();
						metricReq.withNamespace("MicronMetrics");
                    	metricReq.withMetricData(new MetricDatum().withMetricName("MessagesPutToHBase").withTimestamp(new Date()).withValue((double) toGoMessages.size()));
                    	client.putMetricData(metricReq);
					} catch (Exception e) {
						e.printStackTrace();
						LOG.error(e);
					}
				}
			});
			LOG.info("Flushing to Hbase done!");
			
		}
	    
	    
	 
	}
	
	public Result get(String strTable, String rowid) throws IOException {
		// Instantiating HTable class
	      // HTable table = new HTable(config, tableName);
		  TableName tableName = TableName.valueOf(strTable);
		  Table table = conn.getTable(tableName);

	      // Instantiating Get class
	      Get g = new Get(Bytes.toBytes(rowid));
	      
	      // g.addFamily(Bytes.toBytes("cf"));

	      // Reading the data
	      Result result = table.get(g);

	      System.out.println("Result value: " + result);
	      table.close();
	      
	      return result;
	}

}
