package com.micron.poc.dao;

import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.spec.GetItemSpec;
import com.amazonaws.services.dynamodbv2.document.spec.UpdateItemSpec;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ConditionalCheckFailedException;
import com.amazonaws.services.dynamodbv2.model.PutItemRequest;
import com.amazonaws.services.dynamodbv2.model.ReturnValue;
import com.micron.poc.utils.Constants;
import com.micron.poc.utils.MicronUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class DynamoDBConnector {

    private static DynamoDBConnector instance = null;
    static AmazonDynamoDBClient client;
    private static final Log LOG = LogFactory.getLog(DynamoDBConnector.class);
    
    private ConcurrentHashMap<String, Integer> rowkeyCounter = new ConcurrentHashMap<>();
    
    private AtomicLong counter = new AtomicLong();
    
    private ReadWriteLock lock = new ReentrantReadWriteLock();

    private DynamoDBConnector() {
        AWSCredentials credentials = null;
        try {
            credentials = new ProfileCredentialsProvider("default").getCredentials();
        } catch (Exception e) {
            throw new AmazonClientException(
                    "Cannot load the credentials from the credential profiles file. " +
                            "Please make sure that your credentials file is at the correct " +
                            "location (C:\\Users\\manht\\.aws\\credentials), and is in valid format.",
                    e);
        }
        client = new AmazonDynamoDBClient(credentials);

        Region reg = MicronUtils.getRegion();
        client.setRegion(reg);
    }

    public static DynamoDBConnector getInstance() {
        if (instance == null) {
            instance = new DynamoDBConnector();
        }
        return instance;
    }


    /**
     * To increase rowkey counter
     * @param key
     */
    public void increaseChartOOC(String key) {
        DynamoDB    dynamoDB = new DynamoDB(client);
        Table       table    = dynamoDB.getTable(Constants.DYNAMODB_CHART_TABLE_NAME);
        GetItemSpec spec     = new GetItemSpec().withPrimaryKey("chartid", key);
        Item        outcome  = table.getItem(spec);

        if (outcome != null) {
            UpdateItemSpec updateItemSpec = new UpdateItemSpec()
                    .withPrimaryKey("chartid", key)
                    .withUpdateExpression("set OOC_Counter = OOC_Counter + :count")
                    .withValueMap(new ValueMap().withNumber(":count", 1))
                    .withReturnValues(ReturnValue.UPDATED_NEW);
            try {
                table.updateItem(updateItemSpec);
            } catch (Exception e) {
                System.err.println("Unable to update counter");
                System.err.println(e.getMessage());
            }
        } else {

            PutItemRequest request = new PutItemRequest();
            request.setTableName(Constants.DYNAMODB_CHART_TABLE_NAME);
            Map<String, AttributeValue> item   = new HashMap<String, AttributeValue>();
            AttributeValue              attKey = new AttributeValue();
            attKey.setS(key);
            item.put("chartid", attKey);

            AttributeValue attCount = new AttributeValue();
            attCount.setN("1");
            AttributeValue attCount2 = new AttributeValue();
            attCount2.setN("0");
            AttributeValue attAnalyzed = new AttributeValue();
            attAnalyzed.setBOOL(false);
            item.put("OOC_Counter", attCount);
            item.put("Normal_Counter", attCount2);
            item.put("Analyzed", attAnalyzed);
            request.setItem(item);
            try{
                request.setConditionExpression("attribute_not_exists(rowkey)");
                client.putItem(request);
            } catch (ConditionalCheckFailedException e) {
                increaseChartOOC(key);
            }
        }
    }
    
    public void increaseChartNormal(String key) {
        DynamoDB    dynamoDB = new DynamoDB(client);
        Table       table    = dynamoDB.getTable(Constants.DYNAMODB_CHART_TABLE_NAME);
        GetItemSpec spec     = new GetItemSpec().withPrimaryKey("chartid", key);
        Item        outcome  = table.getItem(spec);

        if (outcome != null) {
            UpdateItemSpec updateItemSpec = new UpdateItemSpec()
                    .withPrimaryKey("chartid", key)
                    .withUpdateExpression("set Normal_Counter = Normal_Counter + :count")
                    .withValueMap(new ValueMap().withNumber(":count", 1))
                    .withReturnValues(ReturnValue.UPDATED_NEW);
            try {
                table.updateItem(updateItemSpec);
            } catch (Exception e) {
                System.err.println("Unable to update counter");
                System.err.println(e.getMessage());
            }
        } else {

            PutItemRequest request = new PutItemRequest();
            request.setTableName(Constants.DYNAMODB_CHART_TABLE_NAME);
            Map<String, AttributeValue> item   = new HashMap<String, AttributeValue>();
            AttributeValue              attKey = new AttributeValue();
            attKey.setS(key);
            item.put("chartid", attKey);

            AttributeValue attCount = new AttributeValue();
            attCount.setN("0");
            AttributeValue attCount2 = new AttributeValue();
            attCount2.setN("1");
            AttributeValue attAnalyzed = new AttributeValue();
            attAnalyzed.setBOOL(false);
            item.put("OOC_Counter", attCount);
            item.put("Normal_Counter", attCount2);
            item.put("Analyzed", attAnalyzed);
            request.setItem(item);
            try{
                request.setConditionExpression("attribute_not_exists(rowkey)");
                client.putItem(request);
            } catch (ConditionalCheckFailedException e) {
                increaseChartNormal(key);
            }
        }
    }

    public long getNumberOfRow() {

        DynamoDB dynamoDB = new DynamoDB(client);

        Table table = dynamoDB.getTable(Constants.DYNAMODB_TABLE_NAME);

        GetItemSpec spec = new GetItemSpec().withPrimaryKey("name", "number_of_row");
        Item outcome = table.getItem(spec);
        long number = outcome.getNumber("numberOfRow").longValue();

        return number;
    }

    /**
     * To get Row Key Count
     * @param key
     * @return
     */
    public long getRowKeyCount(String key) {
        DynamoDB    dynamoDB = new DynamoDB(client);
        Table       table    = dynamoDB.getTable(Constants.DYNAMODB_TABLE_NAME);
        GetItemSpec spec     = new GetItemSpec().withPrimaryKey("rowkey", key);
        Item        outcome  = table.getItem(spec);

        if (outcome != null) {
            return outcome.getNumber("rowcounter").longValue();
        }

        return -1L;
    }
    
    /**
     * To increase rowkey counter
     * @param key
     */
    public void increase(String key) {
        long currentValue = counter.incrementAndGet();
        
        Integer oldVal, newVal;
        do {
            // oldVal = rowkeyCounter.get(key);
        	oldVal = rowkeyCounter.putIfAbsent(key, new Integer(0));
        } while (oldVal != null && !rowkeyCounter.replace(key, oldVal, (oldVal == null) ? 1 : (oldVal + 1)));

        if (currentValue % 1000 == 0) {
        	// flush the cache
        	ConcurrentHashMap<String, Integer> tmp = rowkeyCounter;
        	rowkeyCounter = new ConcurrentHashMap<>();
        	for (String k: tmp.keySet()) {
                // Now write to DynamoDB
        		Integer oldVal1 = tmp.get(k);
                if (oldVal1.intValue() > 0) {
                	increase2(k, oldVal1);
                }
        	}
        	LOG.info("Increase successfuly for key: " + key);
        }
    }
    
    /**
     * To increase rowkey counter
     * @param key
     */
    public void increase2(String key, Integer val) {
        DynamoDB    dynamoDB = new DynamoDB(client);
        Table       table    = dynamoDB.getTable(Constants.DYNAMODB_TABLE_NAME);
        GetItemSpec spec     = new GetItemSpec().withPrimaryKey("rowkey", key);
        Item        outcome  = table.getItem(spec);

        if (outcome != null) {
            UpdateItemSpec updateItemSpec = new UpdateItemSpec()
                    .withPrimaryKey("rowkey", key)
                    .withUpdateExpression("set rowcounter = rowcounter + :count")
                    .withValueMap(new ValueMap().withNumber(":count", val.intValue()))
                    .withReturnValues(ReturnValue.UPDATED_NEW);
            try {
                table.updateItem(updateItemSpec);
            } catch (Exception e) {
                System.err.println("Unable to update counter");
                System.err.println(e.getMessage());
            }
        } else {

            PutItemRequest request = new PutItemRequest();
            request.setTableName(Constants.DYNAMODB_TABLE_NAME);
            Map<String, AttributeValue> item   = new HashMap<String, AttributeValue>();
            AttributeValue              attKey = new AttributeValue();
            attKey.setS(key);
            item.put("rowkey", attKey);

            //
            AttributeValue attCount = new AttributeValue();
            attCount.setN(String.valueOf(val.intValue()));
            item.put("rowcounter", attCount);
            request.setItem(item);
            try{
                request.setConditionExpression("attribute_not_exists(rowkey)");
                client.putItem(request);
            } catch (ConditionalCheckFailedException e) {
                increase2(key, val);
            }
        }
    }
}
