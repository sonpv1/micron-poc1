package com.micron.poc.utils;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;

/**
 * Created by manht on 3/25/2017.
 */
public class MicronUtils {
    public static Region getRegion() {
        return Region.getRegion(Regions.US_WEST_1);
    }
}
