package com.micron.poc.utils;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.micron.poc.sns.SnsTopicPublisher;

/**
 * Provides utilities for retrieving SQS client
 */
public class SqsUtils {

	private static final Log LOG = LogFactory.getLog(SqsUtils.class);

	public static AmazonSQS getSqsClient() {
		try {
			AWSCredentials credentials = CredentialUtils.getCredentialsProvider().getCredentials();

			AmazonSQS sqs = AmazonSQSClientBuilder.standard()
					.withCredentials(new AWSStaticCredentialsProvider(credentials))
					.build();
			
			return sqs;
		} catch (Exception ex) {
			LOG.error("Error while get SQS client" + ex);
			return null;
		}
	}
	
}
