package com.micron.poc.utils;

import static com.micron.poc.utils.PropertyLoader.getInstance;

public final class Constants {

	public static final String HBASE_TABLE_NAME = getInstance().getPropValue("hbasetable");
	public static final String DYNAMODB_TABLE_NAME = getInstance().getPropValue("tablename");
	public static final String DYNAMODB_CHART_TABLE_NAME = getInstance().getPropValue("tablename_chart");
	public static final String MICRON_TOPIC_ARN = getInstance().getPropValue("topicArn");
	public static final String MICRON_TOPIC_MSG = "A new record is processed by KCL";

}
