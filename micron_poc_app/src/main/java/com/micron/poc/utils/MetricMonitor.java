package com.micron.poc.utils;

import com.micron.poc.dao.DynamoDBConnector;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by pi on 2/12/17.
 */
public class MetricMonitor extends Thread {

    private static volatile boolean           running             = true;
    private static final    Log               LOG                 = LogFactory.getLog(MetricMonitor.class);
    private static final    DynamoDBConnector DYNAMO_DB_CONNECTOR = DynamoDBConnector.getInstance();
    private static final    String            NAME_SPACE          = "MicronCounter";
    private static final    String            OOC_RECORD          = "oocRecord";
    private static final    String            NORMAL_RECORD       = "normalRecord";
    private static final    String            TOTAL_RECORD        = "totalRecord";
    private static final    SimpleDateFormat  SDF                 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    private long lastOocRecordCounter    = -1;
    private long lastNormalRecordCounter = -1;
    private long lastTotalRecordCounter  = -1;

    @Override
    public void run() {
        SDF.setTimeZone(TimeZone.getTimeZone("UTC"));
        String time   = SDF.format(new Date()) + ".000Z";
        long   ooc    = DYNAMO_DB_CONNECTOR.getRowKeyCount(OOC_RECORD);
        long   normal = DYNAMO_DB_CONNECTOR.getRowKeyCount(NORMAL_RECORD);
        long   total  = DYNAMO_DB_CONNECTOR.getRowKeyCount(TOTAL_RECORD);
//            LOG.info(time + " - " + " OOC: " + ooc + ";NORMAL: " + normal + ";TOTAL: " + total);
        if (ooc >= 0 && ooc > lastOocRecordCounter) {
            lastOocRecordCounter = ooc;
            executeCommand(buildCloudWatchCommand(NAME_SPACE, OOC_RECORD, ooc, time));
        }
        if (normal >= 0 && normal > lastNormalRecordCounter) {
            lastNormalRecordCounter = normal;
            executeCommand(buildCloudWatchCommand(NAME_SPACE, NORMAL_RECORD, normal, time));
        }
        if (total >= 0 && total > lastTotalRecordCounter) {
            lastTotalRecordCounter = total;
            executeCommand(buildCloudWatchCommand(NAME_SPACE, TOTAL_RECORD, total, time));
        }
    }

    /**
     * Stop monitor
     */
    public synchronized void terminate() {
        running = false;
    }

    /**
     * To execute command
     *
     * @param cmd
     * @return
     */
    private static String executeCommand(String cmd) {
        StringBuilder builder = new StringBuilder();
        Process       p       = null;
        try {
            p = Runtime.getRuntime().exec(cmd);
            // cause this process to stop until process p is terminated
            p.waitFor();
            BufferedReader buf  = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String         line = "";
            while ((line = buf.readLine()) != null) {
                builder.append(line).append("\n");
            }
        } catch (Exception e) {
            LOG.warn(e);
        }
        LOG.info(builder);

        return builder.toString();
    }

    private static String buildCloudWatchCommand(String nameSpace, String metricName, long value, String timeStamp) {
        StringBuilder builder = new StringBuilder("aws cloudwatch put-metric-data  --metric-name ").append(metricName);
        builder.append("  --namespace ").append(nameSpace);
        builder.append("  --value ").append(value);
        if (null != timeStamp) {
            builder.append("  --timestamp ").append(timeStamp);
        }
        LOG.info("COMMAND: " + builder.toString());
        System.out.println("COMMAND: " + builder.toString());

        return builder.toString();
    }
}
