package com.micron.poc.sns;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.AmazonSNSClientBuilder;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;
import com.micron.poc.utils.CredentialUtils;
import com.micron.poc.utils.PropertyLoader;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import static com.micron.poc.utils.Constants.MICRON_TOPIC_ARN;

public class SnsTopicPublisher {
	private static final Log    LOG              = LogFactory.getLog(SnsTopicPublisher.class);
	private static AWSCredentials credentials;
	private static AmazonSNS snsClient;

	static {
		try {
			//--- Load default credential in ~/.aws/credentials ---//
			credentials = CredentialUtils.getCredentialsProvider().getCredentials();
			snsClient 	= AmazonSNSClientBuilder.standard().withCredentials(new AWSStaticCredentialsProvider(credentials)).build();
		} catch (Exception ex) {
			LOG.warn(ex);
		}
	}

	/**
	 * Publish message to topic
	 * @param msg
	 */
	public static void publishToTopic(String msg) {
		if (null == snsClient) return;

		try {
			// publish to an SNS topic
			PublishRequest publishRequest = new PublishRequest(MICRON_TOPIC_ARN, msg);
			PublishResult publishResult = snsClient.publish(publishRequest);

			// print MessageId of message published to SNS topic
			LOG.info("MessageId - " + publishResult.getMessageId());
		} catch (Exception e) {
			LOG.warn("Error found while publish to a topic" + MICRON_TOPIC_ARN);
			LOG.warn(e);
		}
	}

}
