package com.micron.poc.sqs;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.cloudwatch.AmazonCloudWatch;
import com.amazonaws.services.cloudwatch.AmazonCloudWatchClientBuilder;
import com.amazonaws.services.cloudwatch.model.MetricDatum;
import com.amazonaws.services.cloudwatch.model.PutMetricDataRequest;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.model.DeleteMessageRequest;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.micron.poc.dao.DynamoDBConnector;
import com.micron.poc.dao.HbaseConnector;
import com.micron.poc.utils.MetricMonitor;
import com.micron.poc.utils.PropertyLoader;
import com.micron.poc.utils.SqsUtils;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.util.Bytes;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map.Entry;

public class SqsReceiverMsg {

	private static final Log LOG = LogFactory.getLog(SqsReceiverMsg.class);

	private static SqsReceiverMsg instance = null;

	private static final String            SQS_QUEUE_NAME      = PropertyLoader.getInstance().getPropValue("sqsQueueName");
	private static final String            SQS_QUEUE_URL       = PropertyLoader.getInstance().getPropValue("sqsUrl");
	private static final String            HBASE_TABLE_NAME    = PropertyLoader.getInstance().getPropValue("hbasetable");
	private static final String            HDFS_PATH    	   = PropertyLoader.getInstance().getPropValue("hdfsPath");
	private static final String            OOC_RECORD          = "oocRecord";
	private static final String            NORMAL_RECORD       = "normalRecord";
	private static final byte[]            FAMILY              = Bytes.toBytes("cf");
	private static final byte[]            QUALIFIER           = Bytes.toBytes("SPC");
	private static final DynamoDBConnector DYNAMO_DB_CONNECTOR = DynamoDBConnector.getInstance();
	private static final HbaseConnector    HBASE_CONNECTOR     = HbaseConnector.getInstance();
	
	private SqsReceiverMsg () {
	}
	
	public static SqsReceiverMsg getInstance() {
		if (instance == null) {
			instance = new SqsReceiverMsg();
		}
		return instance;
	}

	public void sendMsg(String msgBody) {
		// Send a message
		try {
			AmazonSQS sqs = SqsUtils.getSqsClient();

			LOG.info("Sending a message to " + SQS_QUEUE_NAME + " .\n");
			sqs.sendMessage(new SendMessageRequest(SQS_QUEUE_URL, msgBody));
		} catch (AmazonServiceException ase) {
			LOG.debug("Caught an AmazonServiceException, which means your request made it "
					+ "to Amazon SQS, but was rejected with an error response for some reason.");
			LOG.debug("Error Message:    " + ase.getMessage());
		} catch (AmazonClientException ace) {
			LOG.debug("Caught an AmazonClientException, which means the client encountered "
					+ "a serious internal problem while trying to communicate with SQS, such as not "
					+ "being able to access the network.");
			LOG.debug("Error Message: " + ace.getMessage());
		}
	}

	public void receiveMsg() {
		LOG.info("Receiving messages from " + SQS_QUEUE_NAME + ". \n");
		try {
			AmazonSQS sqs = SqsUtils.getSqsClient();
			AmazonCloudWatch client =  AmazonCloudWatchClientBuilder.defaultClient();

			//String hdfsPath = "hdfs://10.0.1.68:8020";
			Configuration conf = new Configuration();
	        conf.set("fs.default.name", HDFS_PATH);
	        conf.set("fs.hdfs.impl", "org.apache.hadoop.hdfs.DistributedFileSystem");
	        conf.set("dfs.replication", "1");
	        
	        FileSystem fileSystem = FileSystem.get(conf);

			boolean flag = true;
			long isProcess = 0;
			String processKey;
			while (flag) {
				ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest(SQS_QUEUE_URL);
				receiveMessageRequest.setMaxNumberOfMessages(10);
				receiveMessageRequest.withMaxNumberOfMessages(1).withWaitTimeSeconds(1);
				List<Message> messages = sqs.receiveMessage(receiveMessageRequest).getMessages();
				for (Message message : messages) {

					LOG.info("  Message");
					LOG.info("    MessageId:     " + message.getMessageId());
					LOG.info("    ReceiptHandle: " + message.getReceiptHandle());
					LOG.info("    MD5OfBody:     " + message.getMD5OfBody());
					LOG.info("    Body:          " + message.getBody());

					processKey = "PROCESSING_" + message.getBody();
					isProcess = DYNAMO_DB_CONNECTOR.getRowKeyCount(processKey);
					if (isProcess >= 0) continue;
					DYNAMO_DB_CONNECTOR.increase(processKey);

					for (Entry<String, String> entry : message.getAttributes().entrySet()) {
						LOG.info("  Attribute");
						LOG.info("    Name:  " + entry.getKey());
						LOG.info("    Value: " + entry.getValue());
					}
					// DYNAMO_DB_CONNECTOR.increase("TOTAL");
					//--- READ FROM HBASE ---//
					Result         result    = HBASE_CONNECTOR.get(HBASE_TABLE_NAME, message.getBody().replaceAll("\"", ""));
					byte[]         value     = result.getValue(FAMILY, QUALIFIER);
					byte[]         chartID     = result.getValue(FAMILY, Bytes.toBytes("chartid"));
					if (chartID != null) LOG.info("Chart ID: " + new String(chartID));
					if (null != value) {
						int spc = Integer.valueOf(new String(value));
						LOG.info("SPC: " + spc);
						PutMetricDataRequest metricReq = new PutMetricDataRequest();
						metricReq.withNamespace("MicronMetrics");
						if (spc < 3 || spc > 8) {
							// DYNAMO_DB_CONNECTOR.increase(OOC_RECORD);
							if (chartID != null) {
								DYNAMO_DB_CONNECTOR.increaseChartOOC(new String(chartID));
							}
							
							metricReq.withMetricData(new MetricDatum().withMetricName("OOC").withTimestamp(new Date()).withValue(1.0));
						} else {
							// DYNAMO_DB_CONNECTOR.increase(NORMAL_RECORD);
							if (chartID != null) {
								DYNAMO_DB_CONNECTOR.increaseChartNormal(new String(chartID));
							}
							
							metricReq.withMetricData(new MetricDatum().withMetricName("Normal").withTimestamp(new Date()).withValue(1.0));
						}
						client.putMetricData(metricReq);
						
						// Now write to the HDFS file system
						// Prepare the data in wide format
						List<String> values = new ArrayList<String>(100);
						for (int i = 3; i < 100; i++) {
							byte[] val = result.getValue(FAMILY, Bytes.toBytes("a" + String.valueOf(i)));
							if (val != null) {
								values.add(new String(val));
							}
						}
						
						String line = StringUtils.join(values, ",");

						if (chartID != null) {
							String chartIDStr = new String(chartID);
							Path path = new Path("/charts/chart-" + chartIDStr + ".txt");

							if (fileSystem.exists(path)) {
								// then append
								FSDataOutputStream out = fileSystem.append(path);
								out.writeChars("\n" + line);
								out.close();
							} else {
								// create new file
								FSDataOutputStream out = fileSystem.create(path);
								out.writeChars(line);
								out.close();
							}
						}

					}
					// new MetricMonitor().start();
					// Delete a message
					deleteMsg(messages);

					if (messages.size() == 0) {
						flag = false;
					}
				}
//				System.out.println();
			}
		} catch (AmazonServiceException ase) {
			LOG.debug("Caught an AmazonServiceException, which means your request made it "
					+ "to Amazon SQS, but was rejected with an error response for some reason.");
			LOG.debug("Error Message:    " + ase.getMessage());
		} catch (AmazonClientException ace) {
			LOG.debug("Caught an AmazonClientException, which means the client encountered "
					+ "a serious internal problem while trying to communicate with SQS, such as not "
					+ "being able to access the network.");
			LOG.debug("Error Message: " + ace.getMessage());
		} catch (Exception ex) {
			LOG.debug(ex);
			ex.printStackTrace();
		}
	}

	public void deleteMsg(List<Message> messages) {
		try {
			AmazonSQS sqs = SqsUtils.getSqsClient();

			LOG.info("Deleting a message.\n");

			String messageReceiptHandle = messages.get(0).getReceiptHandle();
			sqs.deleteMessage(new DeleteMessageRequest(SQS_QUEUE_URL, messageReceiptHandle));
		} catch (AmazonServiceException ase) {
			LOG.debug("Caught an AmazonServiceException, which means your request made it "
					+ "to Amazon SQS, but was rejected with an error response for some reason.");
			LOG.debug("Error Message:    " + ase.getMessage());
		} catch (AmazonClientException ace) {
			LOG.debug("Caught an AmazonClientException, which means the client encountered "
					+ "a serious internal problem while trying to communicate with SQS, such as not "
					+ "being able to access the network.");
			LOG.debug("Error Message: " + ace.getMessage());
		}
	}

	public static void main(String[] args) throws Exception {
		SqsReceiverMsg sqs = getInstance();
		sqs.receiveMsg();
	}

}
