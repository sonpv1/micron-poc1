How To Test

Step1: Run Producer

1. Checkout Producer Code at: https://54.179.165.140/svn/micron_poc/micro-poc-kinesis
2. Execute mvn command: mvn exec:java -Dexec.mainClass="com.fpt.producer.Application"

Step2: Run Consumer
mvn exec:java -Dexec.mainClass="com.micron.poc.kinesis.processor.Consumer"