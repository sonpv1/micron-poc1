from __future__ import print_function

import json

import boto3

print('Loading function')

NUMBER_OF_OOC_RECORDS_THRESHOLD = 3
NUMBER_OF_NORMAL_RECORDS_THRESHOLD = 10
QUEUE_URL = 'https://sqs.us-east-1.amazonaws.com/041209481403/chart_queue'

sqs_client = boto3.client('sqs')

dynamodb_client = boto3.client('dynamodb')

def send_msg_to_queue(queue_url, message_body):
    try:
        # send a message to the queue
        response = sqs_client.send_message(
            QueueUrl=queue_url,
            MessageBody=str(message_body)
        )
        print("Response of SQS: %s" % response)
        return response
    except Exception, e:
        print("Error found while send message to the queue: %s" % e)
        return None

def lambda_handler(event, context):
    #print("Received event: " + json.dumps(event, indent=2))
    for record in event['Records']:
        print(record['eventID'])
        print(record['eventName'])
        print("DynamoDB Record: " + json.dumps(record['dynamodb']['NewImage']['chartid']["S"], indent=2))
        print("DynamoDB Record: " + json.dumps(record['dynamodb']['NewImage']['OOC_Counter']["N"], indent=2))
        print("DynamoDB Record: " + json.dumps(record['dynamodb']['NewImage']['Normal_Counter']["N"], indent=2))
        oocCounter = int(record['dynamodb']['NewImage']['OOC_Counter']["N"])
        normalCounter = int(record['dynamodb']['NewImage']['Normal_Counter']["N"])
        if oocCounter >= NUMBER_OF_OOC_RECORDS_THRESHOLD and normalCounter >= NUMBER_OF_NORMAL_RECORDS_THRESHOLD :
            print('Number of records: %s > threshold: %s. Send message to the queue.'
                % (oocCounter, NUMBER_OF_OOC_RECORDS_THRESHOLD))
            send_msg_to_queue(QUEUE_URL, json.dumps(record['dynamodb']['NewImage']['chartid']["S"]))
        else:
            print('Number of records < threshold. Not send message to the queue.')
    return 'Successfully processed {} records.'.format(len(event['Records']))

