package com.fpt.producer.writer;


import com.fpt.producer.model.Message;
import com.fpt.producer.utils.ProducerUtils;
import com.fpt.producer.utils.RandomStringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import static com.fpt.producer.Constant.*;

/**
 * Created by pi on 2/9/17.
 */
public class Worker extends Thread {

    private int    startRowKey;
    private int    endRowKey;
    private int    startAttribute;
    private int    endAttribute;
    private String name;
    private String chartID;
    private String timestamp;

    private static final Log                    LOG     = LogFactory.getLog(Worker.class);
    private final        BlockingQueue<Message> CACHE   = new LinkedBlockingQueue<>();
    private final        MessageWriter          WRITER  = new MessageWriter();
    private final static Random                 random  = new Random();
    private volatile     boolean                running = true;
    private              Monitor                monitor = new Monitor();


    public Worker(String chartID, String timestamp, int startRowKey, int endRowKey, int startAttribute, int endAttribute) {
        this.startRowKey    = startRowKey;
        this.startAttribute = startAttribute;
        this.endRowKey      = endRowKey;
        this.endAttribute   = endAttribute;
        this.chartID		= chartID;
        this.timestamp		= timestamp;
        //--- Create thread name ---//
        this.name           = startRowKey + "->" + endRowKey + ":" + startAttribute + "->" + endAttribute;
    }

    @Override
    public void run() {
    	monitor.start();
        String  rowKey  = null;
        Message message = null;
        for (int i = startRowKey; i <= endRowKey; i++) {
            rowKey = "ch" + chartID + ":" + "r" + i + ":" + timestamp + ":" + ProducerUtils.getIpAddress();
            for (int j = startAttribute; j <= endAttribute; j++) {
                message = new Message();
                message.setRowKey(rowKey);
                if (j == SPC_INDEX) {
                    message.setAttribute("SPC");
                    // Random value from 0 -> 10
                    message.setValue(String.valueOf(random.nextInt(11)));
                } else {
                    if (j == CHARDID_INDEX) {
                        // Random chardId in 10 items
                        message.setAttribute("chartid");
                        message.setValue(chartID);
                    } else {
                        // Normal attribute
                        message.setAttribute("a" + j);
                        message.setValue(String.valueOf(random.nextInt(1000)));
                    }
                }
                //--- ADD TO CACHE TO SUBMIT BY BATCH ---//
//                WRITER.sendMessage(message);
                CACHE.add(message);
                
//                try {
//                	Thread.sleep(500);
//                } catch (Exception e) {
//                	
//                }
            }
        }
        while (CACHE.size() > 0) {}
        running = false;
        monitor = null;
        LOG.info(name + " HAS BEEN COMPLETED SUCCESSFULLY!");
        try {
        	monitor.join();
        } catch (Exception e) {
        	e.printStackTrace();
        }
    }

    /**
     * Monitor CACHE
     */
    class Monitor extends Thread {

        /**
         * Monitor the QUEUE to resolve shard limit in Kinesis
         */
        @Override
        public void run() {
            List<Message> messages       = new ArrayList<>();
            long          start          = System.currentTimeMillis();
            int           totalSubmitted = 0;
            while (running) {
                Message message = CACHE.poll();
                if (null != message) {
                    messages.add(message);
                }
                //--- SUBMIT WHEN MEET BATCH SIZE ---///
                if (messages.size() >= BATCH_SIZE) {
                    totalSubmitted += messages.size();
                    WRITER.sendMessages(messages);
                    messages.clear();
                    start = System.currentTimeMillis();
                    LOG.info("MEET BATCH SIZE: " + BATCH_SIZE);
                } else {
                    //--- SUBMIT WHEN  MEET TIMEOUT 5s---//
                    if (System.currentTimeMillis() - start >= 3000 && messages.size() < BATCH_SIZE && messages.size() > 0) {
                        totalSubmitted += messages.size();
                        WRITER.sendMessages(messages);
                        messages.clear();
                        start = System.currentTimeMillis();
                        LOG.info("TIMEOUT AND SUBMITTED: " + messages.size());
                    }
                }
                // CHECK LIMIT SHARD
                while (System.currentTimeMillis() - start < 1000 && totalSubmitted >= SHARD_LIMIT) ;
                if (System.currentTimeMillis() - start >= 1000) {
                    LOG.info(name + "; SUBMITTED: " + totalSubmitted);
                    start = System.currentTimeMillis();
                    totalSubmitted = 0;
                }
            }
        }
    }
}
