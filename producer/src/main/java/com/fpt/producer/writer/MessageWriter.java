/*
 * Copyright 2015 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Amazon Software License (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 * http://aws.amazon.com/asl/
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package com.fpt.producer.writer;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.RegionUtils;
import com.amazonaws.services.kinesis.AmazonKinesis;
import com.amazonaws.services.kinesis.AmazonKinesisClient;
import com.amazonaws.services.kinesis.model.DescribeStreamResult;
import com.amazonaws.services.kinesis.model.PutRecordRequest;
import com.amazonaws.services.kinesis.model.PutRecordsRequest;
import com.amazonaws.services.kinesis.model.PutRecordsRequestEntry;
import com.amazonaws.services.kinesis.model.ResourceNotFoundException;
import com.fpt.producer.Constant;
import com.fpt.producer.model.Message;
import com.fpt.producer.utils.ConfigurationUtils;

/**
 * Continuously sends simulated stock trades to Kinesis
 */
public class MessageWriter {

    private static final Log LOG = LogFactory.getLog(MessageWriter.class);
    private AmazonKinesis kinesisClient;

    public MessageWriter() {
        try {
            Region region = RegionUtils.getRegion(Constant.REGION_NAME);
            if (region == null) {
                System.err.println(Constant.REGION_NAME + " is not a valid AWS region.");
                System.exit(1);
            }

            AWSCredentialsProvider credentials = new DefaultAWSCredentialsProviderChain();
            kinesisClient = new AmazonKinesisClient(credentials, ConfigurationUtils.getClientConfigWithUserAgent());
            kinesisClient.setRegion(region);

            // Validate that the stream exists and is active
            validateStream(kinesisClient, Constant.STREAM_NAME);
        } catch (Exception ex) {
            LOG.warn("Error connecting to Amazon Kinesis.", ex);
        }
    }

    /**
     * Checks if the stream exists and is active
     *
     * @param kinesisClient Amazon Kinesis client instance
     * @param streamName    Name of stream
     */
    private static void validateStream(AmazonKinesis kinesisClient, String streamName) {
        try {
            DescribeStreamResult result = kinesisClient.describeStream(streamName);
            if (!"ACTIVE".equals(result.getStreamDescription().getStreamStatus())) {
                System.err.println("Stream " + streamName + " is not active. Please wait a few moments and try again.");
                System.exit(1);
            }
        } catch (ResourceNotFoundException e) {
            System.err.println("Stream " + streamName + " does not exist. Please create it in the console.");
            System.err.println(e);
            System.exit(1);
        } catch (Exception e) {
            System.err.println("Error found while describing the stream " + streamName);
            System.err.println(e);
            System.exit(1);
        }
    }

    /**
     * Uses the Kinesis client to send the message to the given stream.
     *
     * @param message instance representing the Message
     */
    public void sendMessage(Message message) {
        byte[] bytes = message.toJsonAsBytes();
        // The bytes could be null if there is an issue with the JSON serialization by the Jackson JSON library.
        if (bytes == null) {
            LOG.warn("Could not get JSON bytes for messge");
            return;
        }
        LOG.info("Putting message: " + message.toString());
        PutRecordRequest putRecord = new PutRecordRequest();
        putRecord.setStreamName(Constant.STREAM_NAME);
        // We use the ticker symbol as the partition key, explained in the Supplemental Information section below.
        putRecord.setPartitionKey(message.getRowKey());
        putRecord.setData(ByteBuffer.wrap(bytes));
        try {
            kinesisClient.putRecord(putRecord);
        } catch (AmazonClientException ex) {
            LOG.warn("Error sending record to Amazon Kinesis.", ex);
        }
    }

    /**
     * Uses the Kinesis client to send multiple messages to the given stream.
     *
     * @param messages - The list of message
     */
    public void sendMessages(List<Message> messages) {
        if (messages == null) {
            LOG.warn("Could not get JSON bytes for stock trade");
            return;
        }
        LOG.info("Putting message: " + messages.toString());
        PutRecordsRequest putRecordsRequest = new PutRecordsRequest();
        putRecordsRequest.setStreamName(Constant.STREAM_NAME);
        List <PutRecordsRequestEntry> putRecordsRequestEntryList = new ArrayList<>();
        for (int i = 0; i < messages.size(); i++) {
            PutRecordsRequestEntry putRecordsRequestEntry  = new PutRecordsRequestEntry();
            putRecordsRequestEntry.setData(ByteBuffer.wrap(messages.get(i).toJsonAsBytes()));
            putRecordsRequestEntry.setPartitionKey(messages.get(i).getRowKey());
            putRecordsRequestEntryList.add(putRecordsRequestEntry);
        }
        putRecordsRequest.setRecords(putRecordsRequestEntryList);
        try {
            kinesisClient.putRecords(putRecordsRequest);
        } catch (Exception ex) {
            ex.printStackTrace();
            LOG.warn("Error sending record to Amazon Kinesis.", ex);
        }
    }
}
