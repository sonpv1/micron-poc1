package com.fpt.producer.writer;

import com.fpt.producer.utils.ProducerUtils;
import com.fpt.producer.utils.RandomStringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicLong;

import static com.fpt.producer.Constant.*;

/**
 * Created by pi on 2/9/17.
 */
public class MessageGenerator {

    private final static ExecutorService executor = Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    /**
     * To generate message based on configuration. The configuration value could confuse and hard to understand :)
     */
    public void generate() {
        long start       = System.currentTimeMillis();
        String timestamp = ProducerUtils.getTimeStamp();
        for (int k = 0; k < 10; k++) {
        	String chartID = RandomStringUtils.getChardId(k); 
	        int  startRowKey = START_ROW_KEY;
	        for (int i = 0; i < NUMBER_OF_THREADS; i++) {
	            Thread thread = new Worker(chartID, timestamp, startRowKey, NUMBER_OF_ROW_KEY_IN_THREAD + startRowKey - 1, START_ATTRIBUTE, END_ATTRIBUTE);
	            executor.execute(thread);
	            startRowKey += NUMBER_OF_ROW_KEY_IN_THREAD;
	        }
	
	        
        }
        
        // executor.shutdown();
        while (!executor.isTerminated()) {
        }
        
        System.out.println("Finished all threads");
        System.out.println("Time: " + (System.currentTimeMillis() - start) + " ms");
    }


}
