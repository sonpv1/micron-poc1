package com.fpt.producer;

import static com.fpt.producer.utils.PropertyLoader.getInstance;

/**
 * Created by pi on 2/9/17.
 */
public interface Constant {
    public final static int    SPC_INDEX                   = Integer.parseInt(getInstance().getPropValue("spcIndex"));
    public final static int    CHARDID_INDEX               = Integer.parseInt(getInstance().getPropValue("chardIdIndex"));
    public final static int    BATCH_SIZE                  = Integer.parseInt(getInstance().getPropValue("batchSize"));
    public final static int    SHARD_LIMIT                 = Integer.parseInt(getInstance().getPropValue("shardLimit"));
    public final static int    NUMBER_OF_THREADS           = Integer.parseInt(getInstance().getPropValue("numberOfThreads"));
    public final static int    NUMBER_OF_ROW_KEY_IN_THREAD = Integer.parseInt(getInstance().getPropValue("numberOfRowKeyInThread"));
    public final static int    END_ATTRIBUTE               = Integer.parseInt(getInstance().getPropValue("endAttribute"));
    public final static int    START_ROW_KEY               = Integer.parseInt(getInstance().getPropValue("startRowKey"));
    public final static int    START_ATTRIBUTE             = Integer.parseInt(getInstance().getPropValue("startAttribute"));
    public final static String STEP                        = getInstance().getPropValue("step");
    public final static String STREAM_NAME                 = getInstance().getPropValue("streamName");
    public final static String REGION_NAME                 = getInstance().getPropValue("regionName");
    // public final static String ACCESS_KEY                  = getInstance().getPropValue("accessKey");
    // public final static String SECRET_KEY                  = getInstance().getPropValue("secretKey");

}
