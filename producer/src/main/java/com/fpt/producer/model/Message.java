package com.fpt.producer.model;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

/**
 * Created by Pi on 2/9/17.
 */
public class Message {
    private String rowKey;
    private String attribute;
    private String value;

    private final static ObjectMapper JSON = new ObjectMapper();
    static {
        JSON.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    public byte[] toJsonAsBytes() {
        try {
            return JSON.writeValueAsBytes(this);
        } catch (IOException e) {
            return null;
        }
    }

    public static Message fromJsonAsBytes(byte[] bytes) {
        try {
            return JSON.readValue(bytes, Message.class);
        } catch (IOException e) {
            return null;
        }
    }

    @Override
    public String toString() {
        return rowKey + ";" + attribute + ";" + value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Message message = (Message) o;

        return rowKey != null ? rowKey.equals(message.rowKey) : message.rowKey == null;
    }

    @Override
    public int hashCode() {
        return rowKey != null ? rowKey.hashCode() : 0;
    }

    public String getRowKey() {
        return rowKey;
    }

    public void setRowKey(String rowKey) {
        this.rowKey = rowKey;
    }

    public String getAttribute() {
        return attribute;
    }

    public void setAttribute(String attribute) {
        this.attribute = attribute;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
