package com.fpt.producer.utils;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

/**
 * Created by pi on 2/9/17.
 */
public class JSONUtils {
    private final static ObjectMapper JSON = new ObjectMapper();
    static {
        JSON.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    public static byte[] toJsonAsBytes(Object obj) {
        try {
            return JSON.writeValueAsBytes(obj);
        } catch (IOException e) {
            return null;
        }
    }

    public static Object fromJsonAsBytes(byte[] bytes, Class clzz) {
        try {
            return JSON.readValue(bytes, clzz);
        } catch (IOException e) {
            return null;
        }
    }
}
