package com.fpt.producer.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Responsible for loading properties file
 * Created by pi on 2/9/17.
 */
public class PropertyLoader {

    private InputStream inputStream;
    private Properties prop = new Properties();
    private static final PropertyLoader INSTANCE = new PropertyLoader();

    public static PropertyLoader getInstance() {
        return INSTANCE;
    }

    private PropertyLoader()  {
        try {
            prop = new Properties();
            String propFileName = "config.properties";

            inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);

            if (inputStream != null) {
                prop.load(inputStream);
            } else {
                throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
            }
        } catch (Exception e) {
            System.out.println("Exception: " + e);
        } finally {
            try {inputStream.close();} catch (Exception ex) {}
        }
    }

    /**
     * To get property value by key
     * @param key
     * @return
     */
    public String getPropValue(String key){
        return prop.getProperty(key);
    }

}
