package com.fpt.producer.utils;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by pi on 2/12/17.
 */
public class ProducerUtils {

    private static       String           HOST_NAME = "PRODUCER";
    private static       String           IP        = "127.0.0.1";
    private static final SimpleDateFormat SF        = new SimpleDateFormat("yyyy-MM-dd-HH:mm:ss");

    static {
        try {
            HOST_NAME   = InetAddress.getLocalHost().getHostName();
            IP          = InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

    /**
     * To get host name
     *
     * @return
     */
    public static String getHostName() {
        return HOST_NAME;
    }

    /**
     * To get ip address
     * @return
     */
    public static String getIpAddress() {
        return IP;
    }

    /**
     * To get current timestamp
     *
     * @return
     */
    public static String getTimeStamp() {
        return SF.format(new Date());
    }


}
