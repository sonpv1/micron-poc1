package com.fpt.producer.utils;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by pi on 2/11/17.
 */
public class RandomStringUtils {
    private static final String       AB       = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    private static       SecureRandom rnd      = new SecureRandom();
    private static       List<String> chardIds = new ArrayList<>();

    static {
        for (int i = 0; i < 10; i++) {
            chardIds.add(RandomStringUtils.randomString(8));
        }
    }

    /**
     * To get chard id by index
     *
     * @param index
     * @return
     */
    public static String getChardId(int index) {
        return chardIds.get(index);
    }

    /**
     * To generate string in length randomly
     *
     * @param len
     * @return
     */
    public static String randomString(int len) {
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++)
            sb.append(AB.charAt(rnd.nextInt(AB.length())));

        return sb.toString();
    }
}
