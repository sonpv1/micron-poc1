package com.fpt.producer;

import com.fpt.producer.writer.MessageGenerator;

/**
 * Created by pi on 2/10/17.
 */
public class Application {
    public static void main(String[] args) {
        new MessageGenerator().generate();
    }
}
