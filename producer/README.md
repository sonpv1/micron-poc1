**Amazon Kinesis Streams Producer**

This project is used to generate data randomly and put records to the data stream.
 
**How to build?**<br />
Execute command in root directory of this project: <br />
`mvn clean package`

There are 2 jar files which will be generated in target directory. One of theme is full package which includes all dependencies and is named with postfix "**_jar-with-dependencies_**".

**How to execute the application?**<br />
Firstly, you must copy config.properties from target/classes to the same directory with jar file.
After that, you need to execute the command as below:<br />
`java -jar producer-1.0-SNAPSHOT-jar-with-dependencies.jar`

**Good luck!**