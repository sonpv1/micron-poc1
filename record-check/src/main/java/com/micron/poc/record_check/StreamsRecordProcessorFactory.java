package com.micron.poc.record_check;
import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.kinesis.clientlibrary.interfaces.IRecordProcessor;
import com.amazonaws.services.kinesis.clientlibrary.interfaces.IRecordProcessorFactory;
import com.amazonaws.services.sqs.AmazonSQS;

public class StreamsRecordProcessorFactory implements
        IRecordProcessorFactory {

    // private final AWSCredentialsProvider dynamoDBCredentials;
    // private final String dynamoDBEndpoint;
    // private final String tableName;
	private AmazonSQS sqsClient;

    public StreamsRecordProcessorFactory(AmazonSQS sqsClient,
            String serviceName) {
        // this.dynamoDBCredentials = dynamoDBCredentials;
        // this.dynamoDBEndpoint = dynamoDBEndpoint;
        // this.tableName = tableName;
    	this.sqsClient = sqsClient;
    }

    @Override
    public IRecordProcessor createProcessor() {
        // AmazonDynamoDBClient dynamoDBClient = new AmazonDynamoDBClient(dynamoDBCredentials, new ClientConfiguration());
        // dynamoDBClient.setEndpoint(dynamoDBEndpoint);
        return new StreamsRecordProcessor(this.sqsClient);
    }

}