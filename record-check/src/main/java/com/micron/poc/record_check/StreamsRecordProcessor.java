package com.micron.poc.record_check;

import java.nio.charset.Charset;
import java.util.List;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.streamsadapter.model.RecordAdapter;
import com.amazonaws.services.kinesis.clientlibrary.interfaces.IRecordProcessor;
import com.amazonaws.services.kinesis.clientlibrary.interfaces.IRecordProcessorCheckpointer;
import com.amazonaws.services.kinesis.clientlibrary.types.ShutdownReason;
import com.amazonaws.services.kinesis.model.Record;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.micron.poc.utils.PropertyLoader;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class StreamsRecordProcessor implements IRecordProcessor {

    private Integer checkpointCounter;

    private static final Log LOG = LogFactory.getLog(StreamsRecordProcessor.class);
    private static int THRESHOLD = Integer.parseInt(PropertyLoader.getInstance().getPropValue("threshold"));

    // private final AmazonDynamoDBClient dynamoDBClient;
    // private final String tableName;
    private AmazonSQS sqsClient;
    

    public StreamsRecordProcessor(AmazonSQS sqsClient) {
//        this.dynamoDBClient = dynamoDBClient;
//        this.tableName = tableName;
    	this.sqsClient = sqsClient;
    }

    @Override
    public void initialize(String shardId) {
        checkpointCounter = 0;
    }

    @Override
    public void processRecords(List<Record> records,
            IRecordProcessorCheckpointer checkpointer) {
        LOG.info("Start processing record in DynamoDB Stream: " + records.size());
        System.out.println("Start processing record in DynamoDB Stream: " + records.size());
        for(Record record : records) {
            String data = new String(record.getData().array(), Charset.forName("UTF-8"));
            // System.out.println(data);
            if(record instanceof RecordAdapter) {
                com.amazonaws.services.dynamodbv2.model.Record streamRecord = ((RecordAdapter) record).getInternalObject();
                
                String rowkey = streamRecord.getDynamodb().getNewImage().get("rowkey").getS();
                if (!rowkey.startsWith("ch")) return;
                System.out.println("rowkey: " + rowkey);
                switch(streamRecord.getEventName()) { 
                case "INSERT" : case "MODIFY" :
                    // StreamsAdapterDemoHelper.putItem(dynamoDBClient, tableName, streamRecord.getDynamodb().getNewImage());
                	System.out.println("MODIFY");
                	
                	// Now check the condition
                	int rowkey_counter = Integer.parseInt(streamRecord.getDynamodb().getNewImage().get("rowcounter").getN());
                	
                	if (rowkey_counter == THRESHOLD) {
                        String queueName = PropertyLoader.getInstance().getPropValue("sqsQueueName");
                        System.out.println("Start sending message to queue: " + queueName);
                        String queueURL = sqsClient.getQueueUrl(queueName).getQueueUrl();
	                	sqsClient.sendMessage(new SendMessageRequest(queueURL, rowkey));
                        System.out.println("Finish sending message to queue: " + queueName);
                	} else if (rowkey_counter > THRESHOLD) {
                                System.out.println("ERROR: Counter greater than " + THRESHOLD);
                    }
                    break;
                case "REMOVE" :
                	System.out.println("REMOVE");
                    // StreamsAdapterDemoHelper.deleteItem(dynamoDBClient, tableName, streamRecord.getDynamodb().getKeys().get("Id").getN());
                }
            }
            checkpointCounter += 1;
            if(checkpointCounter % 10 == 0) {
                try {
                    checkpointer.checkpoint();
                } catch(Exception e) {
                    e.printStackTrace();
                }
            }
        }

    }

    @Override
    public void shutdown(IRecordProcessorCheckpointer checkpointer,
            ShutdownReason reason) {
        if(reason == ShutdownReason.TERMINATE) {
            try {
                checkpointer.checkpoint();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
