package com.micron.poc.record_check;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.services.cloudwatch.AmazonCloudWatchClient;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.streamsadapter.AmazonDynamoDBStreamsAdapterClient;
import com.amazonaws.services.kinesis.clientlibrary.interfaces.IRecordProcessorFactory;
import com.amazonaws.services.kinesis.clientlibrary.lib.worker.InitialPositionInStream;
import com.amazonaws.services.kinesis.clientlibrary.lib.worker.KinesisClientLibConfiguration;
import com.amazonaws.services.kinesis.clientlibrary.lib.worker.Worker;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.micron.poc.utils.PropertyLoader;
import com.micron.poc.utils.MicronUtil;

public class StreamAdapterRecordChecker {
	private static Worker worker;
    private static KinesisClientLibConfiguration workerConfig;
    private static IRecordProcessorFactory recordProcessorFactory;

    private static AmazonDynamoDBStreamsAdapterClient adapterClient;
    private static AWSCredentialsProvider streamsCredentials;
    private static AWSCredentialsProvider sqsCredentials;

    private static AmazonDynamoDBClient dynamoDBClient;
    private static AWSCredentialsProvider dynamoDBCredentials;

    private static AmazonCloudWatchClient cloudWatchClient;

    private static String serviceName = "dynamodb";
    private static String DYNAMODB_ENDPOINT = PropertyLoader.getInstance().getPropValue("dynamodbEndpoint");
    private static String STREAM_ENDPOINT = PropertyLoader.getInstance().getPropValue("streamsEndpoint");
    private static String tablePrefix = "KCL-Demo";
    private static String STREAM_ARN = PropertyLoader.getInstance().getPropValue("streamArn");

    /**
     * @param args
     */
    public static void main(String[] args) throws Exception {
        System.out.println("Starting demo...");

//        String srcTable = tablePrefix + "-src";
//        String destTable = tablePrefix + "-dest";
        streamsCredentials = new ProfileCredentialsProvider();
        sqsCredentials = new ProfileCredentialsProvider();
        dynamoDBCredentials = new ProfileCredentialsProvider();
        
        AmazonSQS sqs = new AmazonSQSClient(sqsCredentials);
        Region reg = MicronUtil.getRegion();
        sqs.setRegion(reg);
        
        recordProcessorFactory = new StreamsRecordProcessorFactory(sqs, serviceName);
        


        /* ===== REQUIRED =====
         * Users will have to explicitly instantiate and configure the adapter, then pass it to
         * the KCL worker.
         */
        adapterClient = new AmazonDynamoDBStreamsAdapterClient(streamsCredentials, new ClientConfiguration());
        adapterClient.setEndpoint(STREAM_ENDPOINT);

        dynamoDBClient = new AmazonDynamoDBClient(dynamoDBCredentials, new ClientConfiguration());
        dynamoDBClient.setEndpoint(DYNAMODB_ENDPOINT);

        cloudWatchClient = new AmazonCloudWatchClient(dynamoDBCredentials, new ClientConfiguration());
        
        

        // setUpTables();

        workerConfig = new KinesisClientLibConfiguration("streams-adapter-demo",
                STREAM_ARN, streamsCredentials, "streams-demo-worker")
            .withMaxRecords(1000)
            .withIdleTimeBetweenReadsInMillis(500)
            .withInitialPositionInStream(InitialPositionInStream.LATEST);

        System.out.println("Creating worker for stream: " + STREAM_ARN);
        worker = new Worker(recordProcessorFactory, workerConfig, adapterClient, dynamoDBClient, cloudWatchClient);
        System.out.println("Starting worker...");
        Thread t = new Thread(worker);
        t.start();

        Thread.sleep(30000000);
        worker.shutdown();
        t.join();

//        if(StreamsAdapterDemoHelper.scanTable(dynamoDBClient, srcTable).getItems().equals(StreamsAdapterDemoHelper.scanTable(dynamoDBClient, destTable).getItems())) {
//            System.out.println("Scan result is equal.");
//        } else {
//            System.out.println("Tables are different!");
//        }

        System.out.println("Done.");
        // cleanupAndExit(0);
    }

//    private static void setUpTables() {
//        String srcTable = tablePrefix + "-src";
//        String destTable = tablePrefix + "-dest";
//        streamArn = StreamsAdapterDemoHelper.createTable(dynamoDBClient, srcTable);
//        StreamsAdapterDemoHelper.createTable(dynamoDBClient, destTable);
//
//        awaitTableCreation(srcTable);
//
//        performOps(srcTable);
//    }

//    private static void awaitTableCreation(String tableName) {
//        Integer retries = 0;
//        Boolean created = false;
//        while(!created && retries < 100) {
//            DescribeTableResult result = StreamsAdapterDemoHelper.describeTable(dynamoDBClient, tableName);
//            created = result.getTable().getTableStatus().equals("ACTIVE");
//            if (created) {
//                System.out.println("Table is active.");
//                return;
//            } else {
//                retries++;
//                try {
//                    Thread.sleep(1000);
//                } catch(InterruptedException e) {
//                    // do nothing
//                }
//            }
//        }
//        System.out.println("Timeout after table creation. Exiting...");
//        cleanupAndExit(1);
//    }
//
//    private static void performOps(String tableName) {
//    	StreamsAdapterDemoHelper.putItem(dynamoDBClient, tableName, "101", "test1");
//        StreamsAdapterDemoHelper.updateItem(dynamoDBClient, tableName, "101", "test2");
//        StreamsAdapterDemoHelper.deleteItem(dynamoDBClient, tableName, "101");
//        StreamsAdapterDemoHelper.putItem(dynamoDBClient, tableName, "102", "demo3");
//        StreamsAdapterDemoHelper.updateItem(dynamoDBClient, tableName, "102", "demo4");
//        StreamsAdapterDemoHelper.deleteItem(dynamoDBClient, tableName, "102");
//    }
//
//    private static void cleanupAndExit(Integer returnValue) {
//        String srcTable = tablePrefix + "-src";
//        String destTable = tablePrefix + "-dest";
//        dynamoDBClient.deleteTable(new DeleteTableRequest().withTableName(srcTable));
//        dynamoDBClient.deleteTable(new DeleteTableRequest().withTableName(destTable));
//        System.exit(returnValue);
//    }

}
